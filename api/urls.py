from rest_framework import routers
from .viewsets import QuestionnaireViewset,ContactViewset

router = routers.SimpleRouter()
router.register('questionnaires', QuestionnaireViewset)
router.register('contacts', ContactViewset)

urlpatterns = router.urls
