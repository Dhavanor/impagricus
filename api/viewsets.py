from rest_framework import viewsets
from .models import Questionnaire,Contact
from .serializers import QuestionnaireSerializer,ContactSerializer
from rest_framework.decorators import action
from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from django.http import HttpResponse

import io
import json
import xlsxwriter
import requests

class QuestionnaireViewset(viewsets.ModelViewSet):
    queryset = Questionnaire.objects.all()
    serializer_class = QuestionnaireSerializer

    @action(detail=False, methods=['get'])
    def by_user(self, request):
        user = self.request.user
        questionnaires = Questionnaire.objects.filter(user_id__exact=user).last()
        serializer = self.get_serializer(questionnaires)
        return Response(serializer.data)
    
    @action(detail=False, methods=['get'])
    def export_excel(self, request):

        # Create an in-memory output file for the new workbook.
        output = io.BytesIO()
        user = self.request.user
        questionnaires = Questionnaire.objects.filter(user_id__exact=user).last()
        serializer = self.get_serializer(questionnaires)

        d = serializer.data

        workbook = xlsxwriter.Workbook(output)
        worksheet = workbook.add_worksheet()
        cell_format = workbook.add_format({'bold': True,'align': 'center'})
        worksheet.center_horizontally()
        worksheet.center_vertically()
        worksheet.set_column('A:Z',20)
        worksheet.set_row(0,None,cell_format)
        worksheet.set_row(1,None,cell_format)

        row = 0
        col = 1

        for question_id in d.keys():
            if question_id != 'closed' and question_id!='user':
                
                answer = d[question_id]
                if type(answer) is dict:
                    n_periods = len(answer)
                    if type(next(iter(answer.values()))) is dict:
                        n_period_details = len(next(iter(answer.values())))
                        worksheet.merge_range(row, col, row, col +(n_periods * n_period_details) - 1, question_id)
                    else:
                        worksheet.merge_range(row, col, row, col + n_periods - 1, question_id)

                    for answer_period in answer:
                        answer_period_details = answer[answer_period]
                        if type(answer_period_details) is dict:
                            worksheet.merge_range(row + 1, col, row + 1, col+len(answer_period_details)-1, answer_period)     
                            for detail in answer_period_details:
                                worksheet.write(row + 2, col, detail)
                                worksheet.write(row + 3, col, answer_period_details[detail])
                                col += 1
                        else:
                            worksheet.merge_range(row + 1, col, row + 2, col, answer_period)
                            worksheet.write(row + 3, col, answer_period_details)
                            col += 1
                else:
                    worksheet.merge_range(row, col, row+2, col, question_id)
                    worksheet.write(row + 2, col, question_id)
                    worksheet.write(row + 3, col, answer)
                    col += 1

        workbook.close()


        # Rewind the buffer.
        output.seek(0)

        # Set up the Http response.
        response = HttpResponse(
            output,
            content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        )
        response['Content-Disposition'] = 'attachment; filename=answers_excel.xlsx'
        return response

class ContactViewset(viewsets.ModelViewSet):
    queryset = Contact.objects.all()
    serializer_class = ContactSerializer

