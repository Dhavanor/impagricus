from rest_framework import serializers
from .models import Questionnaire,Contact
from django.core.mail import send_mail
from rest_auth.serializers import PasswordResetSerializer

class QuestionnaireSerializer(serializers.ModelSerializer):
    class Meta:
        model = Questionnaire
        fields = '__all__'

class ContactSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contact
        fields = '__all__'

    def create(self, validate_data):
        instance = super(ContactSerializer, self).create(validate_data)
        send_mail(
            'Contact email from {}'.format(validate_data.get('name')),
            'Message:\n\n {}. \n\n Contact email: {}'.format(validate_data.get('message'),validate_data.get('email')),
            validate_data.get('email'),
            ['impagricuz.bz@gmail.com'],
            fail_silently=False,
        )
        return instance


class CustomPasswordResetSerializer(PasswordResetSerializer):
    def get_email_options(self) :
      
        return {
            'email_template_name': 'account/email/password_reset_email.txt'
        }
