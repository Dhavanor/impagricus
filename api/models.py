from django.db import models
from django.conf import settings

# Create your models here.
class Questionnaire(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, blank=True, default=None)
    question1 = models.CharField(max_length=50)
    question2 = models.FloatField(blank=True, null=True)
    question3 = models.FloatField(blank=True, null=True)
    question4 = models.FloatField(blank=True, null=True)
    question5 = models.DateField(blank=True, null=True)
    question6 = models.JSONField(blank=True, null=True)
    question7 = models.JSONField(blank=True, null=True)
    question8 = models.JSONField(blank=True, null=True)
    question9 = models.JSONField(blank=True, null=True)
    question10 = models.JSONField(blank=True, null=True)
    question11 = models.JSONField(blank=True, null=True)
    question12 = models.JSONField(blank=True, null=True)
    question13 = models.JSONField(blank=True, null=True)
    question14 = models.JSONField(blank=True, null=True)
    question15 = models.JSONField(blank=True, null=True)
    question16 = models.JSONField(blank=True, null=True)
    question17 = models.JSONField(blank=True, null=True)
    question18 = models.JSONField(blank=True, null=True)
    question19 = models.JSONField(blank=True, null=True)
    question20 = models.JSONField(blank=True, null=True)
    question21 = models.JSONField(blank=True, null=True)
    question22 = models.JSONField(blank=True, null=True)
    question23 = models.JSONField(blank=True, null=True)
    question24 = models.JSONField(blank=True, null=True)
    question25 = models.JSONField(blank=True, null=True)
    question26 = models.JSONField(blank=True, null=True)
    question27 = models.JSONField(blank=True, null=True)
    question28 = models.JSONField(blank=True, null=True)
    question29 = models.JSONField(blank=True, null=True)
    closed = models.BooleanField(default=0)

class Contact(models.Model):
    name = models.CharField(max_length=50)
    email = models.EmailField(max_length=254)
    message = models.TextField()