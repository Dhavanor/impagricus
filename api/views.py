from django.shortcuts import render

# Create your views here.
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

import json
import xlsxwriter
import requests

@api_view()
def null_view(request):
    return Response(status=status.HTTP_400_BAD_REQUEST)

@api_view()
def complete_view(request):
    return Response("Email account is activated")

@api_view()
def export_excel(request):
    response = requests.get(url, params)
    d = json.loads(response.text)

    workbook = xlsxwriter.Workbook('data.xlsx')
    worksheet = workbook.add_worksheet()

    row = 0
    col = 0

    for key in d.keys():
        row += 1
        worksheet.write(row, col, json.dumps(key))
        for item in d[key]:
            worksheet.write(row, col + 1, json.dumps(item))
            row += 1

    workbook.close()